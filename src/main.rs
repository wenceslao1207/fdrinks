use std::string::String;
use std::env;
use std::error::Error;
use scraper::{Html, Selector};
use serde::Serialize;

#[derive(Serialize, Debug)]
struct Category {
    name: String,
    items: Vec<Product>,
    count: usize
}

#[derive(Serialize, Debug)]
struct Product {
    title: String,
    price: String
}

async fn get_fulldrinks_html() -> Result<String, Box<dyn Error>> {
    let body = reqwest::get("https://minitienda.com.ar/administracion/minitiendabackend/api/get-products/54")
        .await?
        .text()
        .await?
        .to_string();
    Ok(body)
}

fn get_lists(input: String) -> Result<Vec<String>, Box<dyn Error>> {
   let document = Html::parse_document(input.as_str());
   let selector = Selector::parse(r#"div.vista-listado"#)
       .unwrap();

   let lists_collection: Vec<String> = document.select(&selector)
       .map(|x| {
            x.inner_html()
                .trim()
                .to_string()
       })
       .collect();
    
    Ok(lists_collection)
}

fn get_price(input: String) -> Result<String, Box<dyn Error>> {
   let document = Html::parse_document(input.as_str());
   let selector = Selector::parse(r#"div.precio"#)
       .unwrap();

   let price = document.select(&selector)
       .next()
       .unwrap()
       .inner_html()
       .trim()
       .to_string();
    Ok(price)
}

fn get_category(input: String) -> Result<String, Box<dyn Error>> {
   let document = Html::parse_document(input.as_str());
   let selector = Selector::parse(r#"span.category-name"#)
       .unwrap();
    let categorie_name = document.select(&selector)
        .next()
        .unwrap()
        .inner_html()
        .trim()
        .to_string();
    Ok(categorie_name)
}

fn get_title(input: String) -> Result<String, Box<dyn Error>> {
   let document = Html::parse_document(input.as_str());
   let selector = Selector::parse(r#"span.producto-titulo"#)
       .unwrap();

    let title = document.select(&selector)
        .next()
        .unwrap()
        .inner_html()
        .trim()
        .to_string();
    Ok(title)
}

fn get_items(input: String) -> Result<Vec<String>, Box<dyn Error>> {
   let document = Html::parse_document(input.as_str());
   let selector = Selector::parse(r#"div.producto-container2"#)
       .unwrap();

    let items: Vec<String> = document.select(&selector)
        .map( |x| x.inner_html().trim().to_string())
        .collect();
    Ok(items)
}

fn filter_category(vector: Vec<Category>, name: String) -> Result<Category, Box<dyn Error>> {
    let result = vector.into_iter()
        .filter(|x| { 
            let x_name_lower_case = x.name.to_lowercase();
            let name_lower_case = name.to_lowercase();
            x_name_lower_case == name_lower_case
        })
        .next()
        .unwrap();
    Ok(result)
}

async fn full_drinks() -> Result<Vec<Category>, Box<dyn Error>> {
    let resp = get_fulldrinks_html()
        .await?;
    let list_collection = get_lists(resp)
        .unwrap()
        .into_iter();


    let result: Vec<Category> = list_collection.map( |x| {
        let name = get_category(x.to_owned())
            .unwrap();
        let items: Vec<Product> = get_items(x.to_owned())
            .unwrap()
            .into_iter()
            .map( |x| {
                let title = get_title(x.to_string()) 
                    .unwrap();
                let price = get_price(x.to_string())
                    .unwrap();

                Product {
                    title,
                    price
                }
            })
            .collect();
        let count = items.len();

        Category {
            name,
            items,
            count

        }
    })
    .collect();

    Ok(result)
}


#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    let categories = full_drinks()
        .await?;
    if args.len() > 1 {
        let query = &args[1];
        let result: String;

        match query.as_str() {
            "--only-categories" => {
                let only_categories: Vec<String> = categories.into_iter()
                    .map(|x| x.name)
                    .collect();
                result = serde_json::to_string_pretty(&only_categories)
                    .unwrap();
            }
            _ => {
                let category = filter_category(categories, query.to_owned())
                    .unwrap();
                result = serde_json::to_string_pretty(&category)
                    .unwrap();
            }
        }
        println!("{}", result);
        return Ok(());
    }

    let serialized = serde_json::to_string_pretty(&categories)
        .unwrap();
    println!("{}", serialized);
    Ok(())
}

